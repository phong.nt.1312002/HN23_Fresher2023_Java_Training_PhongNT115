package problem4;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadFromObject {
    public static void main(String[] args) {
        try (FileInputStream fis = new FileInputStream("assignment6/problem4/nhanvien.bin");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            NhanVien[] nhanViens = (NhanVien[]) ois.readObject();

            System.out.println("Danh sách nhân viên:");
            for (NhanVien nv : nhanViens) {
               nv.getInformation();
            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Lỗi khi đọc file: " + e.getMessage());
            System.exit(0);
        }
    }
}


