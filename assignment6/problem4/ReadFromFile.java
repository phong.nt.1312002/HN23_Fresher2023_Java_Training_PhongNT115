package problem4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("assignment6/problem4/nhanvien.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                NhanVien nhanViens = new NhanVien();
                String[] parts = line.split(" ");
                if (parts.length == 4) {
                    nhanViens.maNV = parts[0];
                    nhanViens.hoTen = parts[1];
                    nhanViens.tuoi = Integer.parseInt(parts[2]);
                    nhanViens.luong = Float.parseFloat(parts[3]);
                    nhanViens.getInformation();
                }
            }

            reader.close();
        } catch (IOException e) {
            System.out.println("Lỗi khi đọc file: " + e.getMessage());
            System.exit(0);
        }
    }
}

