package problem4;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class WriteToObject {
    public static void main(String[] args) {
        NhanVien[] nhanViens = new NhanVien[3];
        for (int i = 0; i < 3; i++) {
            Scanner scanner = new Scanner(System.in);
            NhanVien nhanVien = new NhanVien();
            System.out.println("Nhập thông tin cho nhân viên " + (i + 1) + ":");
            nhanVien.setInformation();
            nhanViens[i] = new NhanVien(nhanVien.maNV, nhanVien.hoTen, nhanVien.tuoi, nhanVien.luong);
        }

        try (FileOutputStream fos = new FileOutputStream("assignment6/problem4/nhanvien.bin");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(nhanViens);
            System.out.println("Mảng nhân viên đã được ghi vào file nhanvien.bin.");
        } catch (IOException e) {
            System.out.println("Lỗi khi ghi file: " + e.getMessage());
            System.exit(0);
        }
    }
}



