package problem4;

import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Scanner;

public class NhanVien implements Serializable {
    public String maNV;
    public String hoTen;
    public int tuoi;
    public float luong;
    public NhanVien() {
    }

    public NhanVien(String maNV, String hoTen, int tuoi, float luong) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.luong = luong;
    }

    public String getMaNV() {
        return maNV;
    }

    public String getHoTen() {
        return hoTen;
    }

    public int getTuoi() {
        return tuoi;
    }

    public float getLuong() {
        return luong;
    }
    public void getInformation(){
        System.out.println("Mã NV: " + getMaNV());
        System.out.println("Họ tên: " + getHoTen());
        System.out.println("Tuổi: " + getTuoi());
        System.out.println("Lương: " + getLuong());
        System.out.println();
    }
    public void setInformation(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Mã NV: ");
        this.maNV = scanner.next();
        System.out.print("Họ tên: ");
        this.hoTen = scanner.next();
        try{
            System.out.print("Tuổi: ");
            this.tuoi = scanner.nextInt();
            if(tuoi<=0){
                throw new GiaTriDuongException("Tuổi phải là số dương!");
            }
        } catch (InputMismatchException e){
            System.out.println("Tuổi phải là số!");
            System.exit(0);
        } catch (GiaTriDuongException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
        try{
            System.out.print("Lương: ");
            this.luong = scanner.nextFloat();
            if(luong<=0){
                throw new GiaTriDuongException("Lương phải là số dương!");
            }
        } catch (InputMismatchException e){
            System.out.println("Lương phải là số!");
            System.exit(0);
        } catch (GiaTriDuongException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
    }
}

