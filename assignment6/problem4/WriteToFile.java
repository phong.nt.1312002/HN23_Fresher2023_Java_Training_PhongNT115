package problem4;

import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class WriteToFile {
    public static void main(String[] args) {
        try {
            FileWriter writer = new FileWriter("assignment6/problem4/nhanvien.txt");
            for (int i = 0; i < 3; i++) {
                Scanner scanner = new Scanner(System.in);
                NhanVien nhanViens = new NhanVien();
                System.out.println("Nhập thông tin cho nhân viên " + (i + 1) + ":");
                nhanViens.setInformation();
                writer.write(nhanViens.getMaNV() + " " + nhanViens.getHoTen() + " " + nhanViens.getTuoi() + " " + nhanViens.getLuong() + "\n");
            }
            writer.close();
            System.out.println("Thông tin nhân viên đã được ghi vào file nhanvien.txt.");
        } catch (IOException e) {
            System.out.println("Lỗi khi ghi file: " + e.getMessage());
        }
    }
}

