package problem1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class DemSoLuongTuTrongFile {
    public static void main(String[] args) {
        String tenFile = "assignment6\\problem1\\prob1.txt";

        try {
            BufferedReader br = new BufferedReader(new FileReader(tenFile));
            String line;
            Set<String> tuDuocXuatHien = new HashSet<>(); // Sử dụng Set để lưu các từ duy nhất.

            while ((line = br.readLine()) != null) {
                String[] words = line.split("\\s+"); // Tách các từ dựa trên khoảng trắng.

                for (String word : words) {
                    // Chuyển tất cả các ký tự thành chữ thường để không phân biệt chữ hoa và chữ thường.
                    word = word.toLowerCase();
                    tuDuocXuatHien.add(word);
                }
            }

            br.close();

            // In ra số lượng từ duy nhất.
            System.out.println("Số lượng từ duy nhất trong file: " + tuDuocXuatHien.size());
        } catch (IOException e) {
            System.err.println("Lỗi khi đọc file: " + e.getMessage());
        }
    }
}
