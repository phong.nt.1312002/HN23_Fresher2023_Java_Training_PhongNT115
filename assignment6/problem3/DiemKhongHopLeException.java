package problem3;
public class DiemKhongHopLeException extends Exception{
    public DiemKhongHopLeException(String message) {
        super(message);
    }
}
