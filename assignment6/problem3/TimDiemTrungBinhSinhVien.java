package problem3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
public class TimDiemTrungBinhSinhVien {
    public static void main(String[] args) {
        // Đường dẫn tới file sinhvien.txt
        String tenFile = "assignment6/problem3/sinhvien.txt";

        try {
            // Nhập mã sinh viên từ bàn phím
            Scanner scanner = new Scanner(System.in);
            System.out.print("Nhập mã sinh viên: ");
            String maSinhVienTimKiem = scanner.next();
            scanner.close();

            // Đọc dữ liệu từ file sinhvien.txt
            BufferedReader br = new BufferedReader(new FileReader(tenFile));
            String line;

            while ((line = br.readLine()) != null) {
                String[] parts = line.split(" ");
                if (parts.length == 3) {
                    String maSinhVien = parts[0];
                    String hoTen = parts[1];
                    double diemTrungBinh;
                    try {
                        diemTrungBinh = Double.parseDouble(parts[2]);
                        if (diemTrungBinh < 0 || diemTrungBinh > 10) {
                            throw new DiemKhongHopLeException("Điểm trung bình không hợp lệ");
                        }
                    } catch (NumberFormatException e) {
                        System.err.println("Lỗi: Điểm trung bình không phải là số.");
                        br.close();
                        return;
                    } catch (DiemKhongHopLeException e) {
                        System.err.println("Lỗi: " + e.getMessage());
                        br.close();
                        return;
                    }
                    if (maSinhVien.equals(maSinhVienTimKiem)) {
                        System.out.println("Thông tin sinh viên:");
                        System.out.println("Mã sinh viên: " + maSinhVien);
                        System.out.println("Họ tên: " + hoTen);
                        System.out.println("Điểm trung bình: " + diemTrungBinh);
                        br.close();
                        return;
                    }
                }
            }
            br.close();
            System.out.println("Không tìm thấy thông tin cho mã sinh viên " + maSinhVienTimKiem);
        } catch (IOException e) {
            System.err.println("Lỗi khi đọc file: " + e.getMessage());
        }
    }
}

