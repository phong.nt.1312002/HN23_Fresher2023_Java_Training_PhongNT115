package problem5;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class QuanLySinhVien {
    public static void main(String[] args) {
        List<SinhVien> danhSachSinhVien = new ArrayList<>();

        try {
            // Đọc danh sách sinh viên từ file sinhvien.txt
            BufferedReader reader = new BufferedReader(new FileReader("assignment6/problem5/sinhvien.txt"));
            String line;

            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(" ");
                if (parts.length == 5) {
                    String maSV = parts[0];
                    String hoTen = parts[1];
                    int tuoi = Integer.parseInt(parts[2]);
                    String diaChi = parts[3];
                    float diemTrungBinh = Float.parseFloat(parts[4]);
                    danhSachSinhVien.add(new SinhVien(maSV, hoTen, tuoi, diaChi, diemTrungBinh));
                }
            }

            reader.close();
        } catch (IOException e) {
            System.err.println("Lỗi khi đọc file: " + e.getMessage());
        }

        int choice;
        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Chương trình quản lý sinh viên");
            System.out.println("1. Thêm sinh viên");
            System.out.println("2. Sửa sinh viên theo mã");
            System.out.println("3. Xóa sinh viên theo mã");
            System.out.println("4. Sắp xếp sinh viên theo họ tên");
            System.out.println("5. Sắp xếp sinh viên theo điểm trung bình");
            System.out.println("6. Hiển thị thông tin tất cả sinh viên");
            System.out.println("0. Thoát chương trình");
            System.out.print("Chọn một tùy chọn: ");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    SinhVien newSinhVien = new SinhVien();
                    newSinhVien.setInformation();
                    danhSachSinhVien.add(newSinhVien);
                    break;
                case 2:
                    System.out.print("Nhập mã sinh viên cần sửa: ");
                    String maSvSua = scanner.next();
                    for (SinhVien sv : danhSachSinhVien) {
                        if (sv.getMaSV().equals(maSvSua)) {
                            System.out.println("Nhập thông tin mới cho sinh viên:");
                            sv.setInformation();
                            break;
                        }
                        else {
                            System.out.println("Sinh viên không tồn tại!");
                        }
                    }
                    break;
                case 3:
                    System.out.print("Nhập mã sinh viên cần xóa: ");
                    String maSvXoa = scanner.next();
                    danhSachSinhVien.removeIf(sv -> sv.getMaSV().equals(maSvXoa));
                    break;
                case 4:
                    Collections.sort(danhSachSinhVien, Comparator.comparing(SinhVien::getHoTen));
                    break;
                case 5:
                    Collections.sort(danhSachSinhVien, Comparator.comparing(SinhVien::getDiemTB).reversed());
                    break;
                case 6:
                    if(!danhSachSinhVien.isEmpty()){
                        for (SinhVien sv : danhSachSinhVien) {
                            System.out.println(sv.getInformation());
                            System.out.println();
                        }
                    }
                    else {
                        System.out.println("Không tồn tại danh sách sinh viên!");
                    }
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Tùy chọn không hợp lệ.");
            }
        } while (choice != 0);

        // Ghi danh sách sinh viên vào file sinhvien.txt
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("assignment6/problem5/sinhvien.txt"));
            for (SinhVien sv : danhSachSinhVien) {
                writer.write(sv.getMaSV() + " " + sv.getHoTen() + " " + sv.getTuoi() + " " + sv.getDiaChi() + " " + sv.getDiemTB() + "\n");
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Lỗi khi ghi file: " + e.getMessage());
        }
    }
}

