package problem5;
public class DiemKhongHopLeException extends Exception{
    public DiemKhongHopLeException(String message) {
        super(message);
    }
}
