package problem5;

import java.io.Serializable;
import java.util.InputMismatchException;
import java.util.Scanner;

public class SinhVien implements Serializable {
    private String maSV;
    private String hoTen;
    private int tuoi;
    private String diaChi;
    private float diemTrungBinh;
    public SinhVien(){
    }
    public SinhVien(String maSV, String hoTen, int tuoi, String diaChi, float diemTrungBinh) {
        this.maSV = maSV;
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.diaChi = diaChi;
        this.diemTrungBinh = diemTrungBinh;
    }

    public void setInformation() {
        Scanner scanner = new Scanner(System.in);
            System.out.print("Mã SV: ");
            this.maSV = scanner.next();
            System.out.print("Họ tên: ");
            this.hoTen = scanner.next();
            try{
                System.out.print("Tuổi: ");
                this.tuoi = scanner.nextInt();
                if (tuoi <= 0){
                    throw new GiaTriDuongException("Tuổi phải là số dương!");
                }
            }catch (InputMismatchException e){
                System.out.println("Tuổi phải là số!");
                System.exit(0);
            }catch (GiaTriDuongException e){
                System.out.println(e.getMessage());
                System.exit(0);
            }
            scanner.nextLine();
            System.out.print("Địa chỉ: ");
            this.diaChi = scanner.nextLine();
            try{
                System.out.print("Điểm trung bình: ");
                this.diemTrungBinh = scanner.nextFloat();
                if (diemTrungBinh<0 || diemTrungBinh>10){
                    throw new DiemKhongHopLeException("Điểm phải nằm trong khoảng từ 0 đến 10!");
                }
            }catch (InputMismatchException e){
                System.out.println("Điểm trung bình phải là số!");
                System.exit(0);
            }catch (DiemKhongHopLeException e){
                System.out.println(e.getMessage());
                System.exit(0);
            }

    }

    public String getInformation() {
        return "Mã SV: " + maSV + "\nHọ tên: " + hoTen + "\nTuổi: " + tuoi + "\nĐịa chỉ: " + diaChi + "\nĐiểm trung bình: " + diemTrungBinh;
    }

    public String getMaSV() {
        return maSV;
    }

    public String getHoTen() {
        return hoTen;
    }

    public int getTuoi() {
        return tuoi;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public float getDiemTB() {
        return diemTrungBinh;
    }
}

