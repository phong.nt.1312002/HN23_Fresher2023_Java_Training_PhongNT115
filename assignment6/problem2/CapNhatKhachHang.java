package problem2;

import java.io.*;
import java.util.Scanner;
import java.util.InputMismatchException;
public class CapNhatKhachHang {
    public static void main(String[] args) {
        // Đường dẫn tới file khachhang.txt
        String tenFile = "assignment6/problem2/khachhang.txt";

        try {
            // Đọc dữ liệu từ file khachhang.txt
            BufferedReader br = new BufferedReader(new FileReader(tenFile));
            StringBuilder content = new StringBuilder();
            String line;

            // Đọc dữ liệu từ file và lưu vào StringBuilder
            while ((line = br.readLine()) != null) {
                content.append(line).append(System.lineSeparator());
            }
            br.close();

            // Nhập thông tin của khách hàng từ bàn phím
            Scanner scanner = new Scanner(System.in);
            System.out.print("Nhập mã khách hàng: ");
            String maKhachHang = scanner.next();
            System.out.print("Nhập họ tên khách hàng: ");
            String hoTen = scanner.next();
            System.out.print("Nhập số điện thoại: ");
            int soDienThoai = scanner.nextInt();
            scanner.close();

            // Tạo một dòng thông tin mới
            String thongTinMoi = maKhachHang + " " + hoTen + " " + soDienThoai;

            // Thay thế dòng thông tin cũ bằng dòng thông tin mới trong StringBuilder
            String noiDungMoi = content.toString().replaceFirst(".*" + maKhachHang + ".*", thongTinMoi);

            // Ghi đè dữ liệu mới vào file
            FileWriter fw = new FileWriter(tenFile);
            fw.write(noiDungMoi);
            fw.close();

            System.out.println("Thông tin khách hàng đã được cập nhật.");
        } catch (IOException e) {
            System.out.println("Lỗi khi đọc hoặc ghi file: " + e.getMessage());
        } catch (InputMismatchException e){
            System.out.println("Số điện thoại bắt buộc phải là số!");
        }

    }
}


