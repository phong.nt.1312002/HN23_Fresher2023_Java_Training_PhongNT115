package problem5;

public class NullObject {
        public static void main(String[] args) {
            try {
                // Tạo một đối tượng và gán giá trị null cho nó
                Object obj = null;

                // Thử tham chiếu đến đối tượng và gọi một phương thức
                obj.toString();
            } catch (NullPointerException e) {
                System.out.println("Lỗi: NullPointerException đã xảy ra.");
            }
        }
}
