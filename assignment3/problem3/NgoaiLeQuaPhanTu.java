package problem3;

import java.util.Scanner;
public class NgoaiLeQuaPhanTu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] arr = new int[5];

        try {
            System.out.println("Nhập phần tử cần truy cập: ");
            int  i = scanner.nextInt();
            int value = arr[i];
            System.out.println("Giá trị của phần tử thứ "+ i +": " + value);
        // Cố gắng truy cập phần tử thứ 7 (vượt quá kích thước của mảng)
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Lỗi: Bạn đã cố gắng truy cập phần tử vượt quá kích thước của mảng.");
        }
    }
}
