package problem1;
import java.util.InputMismatchException;
import java.util.Scanner;
public class ChiaHaiSoThuc {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double number1, number2;

        try {
            System.out.print("Nhập số thực thứ nhất: ");
            number1 = scanner.nextDouble();
            System.out.print("Nhập số thực thứ hai: ");
            number2 = scanner.nextDouble();
            double result = divide(number1, number2);
            System.out.println("Kết quả phép chia: " + result);
        } catch (InputMismatchException e) {
            System.out.println("Lỗi: Bạn đã nhập không phải là số thực.");
        } catch (ArithmeticException e) {
            System.out.println("Lỗi: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

    public static double divide(double numerator, double denominator) {
        // Xử lý ngoại lệ nếu số chia là 0
        if (denominator == 0) {
            throw new ArithmeticException("Phép chia không hợp lệ. Số chia không thể bằng 0.");
        }
        return numerator / denominator;
    }
}
