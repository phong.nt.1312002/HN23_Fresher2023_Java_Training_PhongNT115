package problem4;
import java.util.Scanner;
public class EpKieuString {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            String str = scanner.nextLine();
            try {
                byte byteValue = Byte.parseByte(str); // Cố gắng ép kiểu chuỗi thành kiểu byte
                System.out.println("Kiểu byte: " + byteValue);
            } catch (NumberFormatException e) {
                System.out.println("Lỗi: Không thể ép kiểu chuỗi thành kiểu byte.");
            }

            try {
                short shortValue = Short.parseShort(str); // Cố gắng ép kiểu chuỗi thành kiểu short
                System.out.println("Kiểu short: " + shortValue);
            } catch (NumberFormatException e) {
                System.out.println("Lỗi: Không thể ép kiểu chuỗi thành kiểu short.");
            }

            try {
                long longValue = Long.parseLong(str); // Cố gắng ép kiểu chuỗi thành kiểu long
                System.out.println("Kiểu long: " + longValue);
            } catch (NumberFormatException e) {
                System.out.println("Lỗi: Không thể ép kiểu chuỗi thành kiểu long.");
            }

            try {
                float floatValue = Float.parseFloat(str); // Cố gắng ép kiểu chuỗi thành kiểu float
                System.out.println("Kiểu float: " + floatValue);
            } catch (NumberFormatException e) {
                System.out.println("Lỗi: Không thể ép kiểu chuỗi thành kiểu float.");
            }
            try {
                double doubleValue = Double.parseDouble(str); // Cố gắng ép kiểu chuỗi thành kiểu double
                System.out.println("Kiểu float: " + doubleValue);
            } catch (NumberFormatException e) {
                System.out.println("Lỗi: Không thể ép kiểu chuỗi thành kiểu double.");
            }
        }

}
