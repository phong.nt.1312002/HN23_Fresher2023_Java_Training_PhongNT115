package problem2;
import java.util.Scanner;
public class KhaiBaoMang {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;

        System.out.print("Nhập số phần tử của mảng: ");
        n = scanner.nextInt();

        int[] array = new int[n];

        try {
            inputArray(scanner, array);
        } catch (ValueIs100Exception e) {
            System.out.println("Lỗi: Phần tử có giá trị 100 đã được nhập.");
            System.out.print("Các phần tử đã nhập: ");
            for (int i = 0; i < e.index; i++) {
                System.out.print(e.values[i] + " ");
            }
        } finally {
            scanner.close();
        }
    }

    public static void inputArray(Scanner scanner, int[] array) throws ValueIs100Exception {
        for (int i = 0; i < array.length; i++) {
            System.out.print("Nhập phần tử thứ " + (i + 1) + ": ");
            array[i] = scanner.nextInt();

            if (array[i] == 100) {
                throw new ValueIs100Exception(i, array);
            }
        }
    }
}



