package problem2;
class ValueIs100Exception extends Exception {
        int index;
        int[] values;
        public ValueIs100Exception(int index, int[] values) {
            this.index = index;
            this.values = new int[index + 1];
            System.arraycopy(values, 0, this.values, 0, index + 1);
        }
    }
