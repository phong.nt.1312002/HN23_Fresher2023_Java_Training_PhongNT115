package problem3;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
public class Bai3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 0;
        try {
            System.out.print("Nhập số phần tử  của mảng: ");
            n = scanner.nextInt();
        }catch (InputMismatchException e){
            System.out.println("n phải là số!");
            return;
        }
        if(n<=0){
            System.out.println("n phải là số nguyên dương");
            return;
        }
        int[] arr = new int[n];
        try {
            for (int i = 0; i < arr.length; i++) {
                System.out.print("Nhập phần tử thứ " + (i + 1) + ": ");
                arr[i] = scanner.nextInt();
            }
        }catch (InputMismatchException e){
            System.out.println("Phần tử của mảng phải là số!");
        }
        Arrays.sort(arr);

        System.out.println("Mảng sau khi sắp xếp tăng dần: " + Arrays.toString(arr));

        int elementToInsert = 0;
        try {
            System.out.print("Nhập phần tử thêm vào mảng: ");
            elementToInsert = scanner.nextInt();// Phần tử thêm vào mảng
        }catch (InputMismatchException e){
            System.out.println("Phần tử thêm vào phải là số!");
            return;
        }
        int[] newArray = insertElement(arr, elementToInsert, n);

        System.out.println("Mảng sau khi thêm phần tử " + elementToInsert + ": " + Arrays.toString(newArray));
    }

    public static int[] insertElement(int[] arr, int element, int n) {
        int[] newArray = new int[n + 1];
        int i = 0;

        while (i < n && arr[i] < element) {
            newArray[i] = arr[i];
            i++;
        }

        newArray[i] = element;

        for (int j = i; j < n; j++) {
            newArray[j + 1] = arr[j];
        }

        return newArray;
    }
}

