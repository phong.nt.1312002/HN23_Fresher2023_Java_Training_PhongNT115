package problem4;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Management contactManagement = new Management();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Contact Management Menu:");
            System.out.println("1. Add Contact");
            System.out.println("2. Edit Contact");
            System.out.println("3. Search Contacts");
            System.out.println("4. Sort Contacts");
            System.out.println("5. Display Contacts");
            System.out.println("6. Exit");
            System.out.print("Enter your choice: ");

            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    String name = null;
                    String phoneNumber = null;
                    name = inputName(name);
                    phoneNumber = inputPhoneNumber(phoneNumber);
                    contactManagement.addContact(name, phoneNumber);
                    break;
                case 2:
                    System.out.print("Enter Old Phone Number: ");
                    String oldPhoneNumber = null;
                    oldPhoneNumber = inputPhoneNumber(oldPhoneNumber);
                    System.out.print("Enter New Phone Number: ");
                    String newPhoneNumber = null;
                    newPhoneNumber = inputPhoneNumber(newPhoneNumber);
                    contactManagement.editContact(oldPhoneNumber, newPhoneNumber);
                    break;
                case 3:
                    System.out.print("Enter Name to Search: ");
                    String searchName = null;
                    searchName = inputName(searchName);
                    contactManagement.searchContacts(searchName);
                    break;
                case 4:
                    contactManagement.sortContactsAlphabetically();
                    System.out.println("Contacts sorted alphabetically.");
                    break;
                case 5:
                    contactManagement.displayContacts();
                    break;
                case 6:
                    System.out.println("Exiting Contact Management.");
                    System.exit(0);
                default:
                    System.out.println("Invalid choice. Please choose a valid option.");
            }
        }
    }
    // Phone number format have to be all number or +...: example +84911434566
    public static boolean checkPhoneNumber(String phoneNumber){
        if(phoneNumber.length()!=10&&phoneNumber.charAt(0)!='+') return false;
        else if(phoneNumber==null)return false;
        else if(phoneNumber.charAt(0)!='+' && (phoneNumber.charAt(0)<'0'||phoneNumber.charAt(0)>'9')  ) return false;
        else if(phoneNumber.length()<10) return false;
        for(int i=1;i<phoneNumber.length();i++){
            if(phoneNumber.charAt(i)<'0'||phoneNumber.charAt(i)>'9') return false;
        }
        return true;
    }
    public static boolean checkName(String name){
        if(name.isEmpty())return false;
        for (int i=0;i<name.length();i++){
            if(name.charAt(i)!=' ') return true;
        }
        return false;
    }
    public static String inputName(String name){
        try {
            System.out.print("Enter Name: ");
            Scanner scanner = new Scanner(System.in);
            name = scanner.nextLine();
            if(!checkName(name)){
                throw new WrongNameFormat("Name can't be empty!");
            }
        } catch (WrongNameFormat e){
            System.out.println(e.getMessage());
            System.exit(0);
        } catch (StringIndexOutOfBoundsException e){
            System.out.println("Name can't be empty");
            System.exit(0);
        }
        return name;
    }
    public static String inputPhoneNumber(String phoneNumber){
        try {
            System.out.print("Enter Phone Number: ");
            Scanner scanner = new Scanner(System.in);
            phoneNumber = scanner.nextLine();
            if(!checkPhoneNumber(phoneNumber)){
                throw new WrongPhoneNumberFormat("Wrong Phone number format! It is all number or +xxxxx...!");
            }
        }catch (WrongPhoneNumberFormat e){
            System.out.println(e.getMessage());
            System.exit(0);
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("Phone number can't be empty!");
            System.exit(0);
        }
        return phoneNumber;
    }
}
