package problem4;

import java.util.ArrayList;

class Management {
    private ArrayList<Contact> contactList;

    public Management() {
        contactList = new ArrayList<>();
    }

    public boolean contactExists(String phoneNumber) {
        for (Contact contact : contactList) {
            if (contact.getPhoneNumber().equals(phoneNumber)) {
                return true;
            }
        }
        return false;
    }

    public void addContact(String name, String phoneNumber) {
        if (contactExists(phoneNumber)) {
            System.out.println("Contact already exists.");
        } else {
            Contact contact = new Contact(name, phoneNumber);
            contactList.add(contact);
            System.out.println("Contact added successfully.");
        }
    }

    public void editContact(String oldPhoneNumber, String newPhoneNumber) {
        boolean oldNumberExists = false;
        boolean newNumberExists = false;

        for (Contact contact : contactList) {
            if (contact.getPhoneNumber().equals(oldPhoneNumber)) {
                oldNumberExists = true;
            }
            if (contact.getPhoneNumber().equals(newPhoneNumber)) {
                newNumberExists = true;
            }
        }

        if (oldNumberExists && !newNumberExists) {
            for (Contact contact : contactList) {
                if (contact.getPhoneNumber().equals(oldPhoneNumber)) {
                    contact.setPhoneNumber(newPhoneNumber);
                    System.out.println("Contact phone number updated successfully.");
                }
            }
        } else {
            System.out.println("Phone number update conditions not met.");
        }
    }

    public void searchContacts(String name) {
        System.out.println("Matching Contacts for '" + name + "':");
        boolean exist = false;
        for (Contact contact : contactList) {
            if (contact.getName().toLowerCase().contains(name.toLowerCase())) {
                System.out.println("Name: " + contact.getName() + ", Phone Number: " + contact.getPhoneNumber());
                exist = true;
            }
        }
        if(exist==false){
            System.out.println("This name is not exist!");
        }
    }

    public void sortContactsAlphabetically() {
        contactList.sort((c1, c2) -> c1.getName().compareTo(c2.getName()));
    }

    public void displayContacts() {
        if(!contactList.isEmpty()){
            for (Contact contact : contactList) {
                System.out.println("Name: " + contact.getName() + ", Phone Number: " + contact.getPhoneNumber());
            }
        }
        else {
            System.out.println("Contact list is empty!");
        }
    }
}
