package problem4;

class Contact {
    private String name;
    private String phoneNumber;
    public Contact(){
    }

    public Contact(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    // Getter cho thuộc tính 'name'
    public String getName() {
        return name;
    }

    // Setter cho thuộc tính 'name'
    public void setName(String name) {
        this.name = name;
    }

    // Getter cho thuộc tính 'phoneNumber'
    public String getPhoneNumber() {
        return phoneNumber;
    }

    // Setter cho thuộc tính 'phoneNumber'
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

