package problem4;

public class WrongPhoneNumberFormat extends Exception{
    public WrongPhoneNumberFormat(String message) {
        super(message);
    }

}
