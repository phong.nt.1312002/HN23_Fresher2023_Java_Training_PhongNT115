package problem1;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Bai1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        // Nhập số nguyên dương n
        System.out.print("Nhập số nguyên dương n: ");
        try {
            n = sc.nextInt();

            if (n <= 0) {
                System.out.println("Vui lòng nhập một số nguyên dương.");
                return;
            }
        } catch (InputMismatchException e){
            System.out.println("n phải là số!");
            System.exit(0);
        }

        // a. Tính tổng các chữ số của n
        int sumOfDigits = calculateSumOfDigits(n);
        System.out.println("a. Tổng các chữ số của n: " + sumOfDigits);

        // b. Phân tích n thành thừa số nguyên tố
        System.out.print("b. Phân tích n thành thừa số nguyên tố: ");
        primeFactorization(n);

        // c. Liệt kê các ước của n
        System.out.print("c. Các ước của n: ");
        listDivisors(n);

        // d. Liệt kê các ước nguyên tố của n
        System.out.print("d. Các ước nguyên tố của n: ");
        listPrimeDivisors(n);
    }

    // Hàm tính tổng các chữ số của n
    public static int calculateSumOfDigits(int n) {
        int sum = 0;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }
        return sum;
    }

    // Hàm phân tích n thành thừa số nguyên tố và in kết quả ra màn hình
    public static void primeFactorization(int n) {
        int divisor = 2;
        while (n > 1) {
            if (n % divisor == 0) {
                System.out.print(divisor + " ");
                n /= divisor;
            } else {
                divisor++;
            }
        }
        System.out.println();
    }

    // Hàm liệt kê các ước của n
    public static void listDivisors(int n) {
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    // Hàm liệt kê các ước nguyên tố của n
    public static void listPrimeDivisors(int n) {
        for (int i = 2; i*i <= n*n; i++) {
            if (n % i == 0 && isPrime(i)) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    // Hàm kiểm tra xem một số có phải là số nguyên tố hay không
    public static boolean isPrime(int n) {
            if (n <= 1) {
                return false;
            }

            // Tạo một mảng để đánh dấu các số từ 2 đến n
            boolean[] isPrime = new boolean[n + 1];
            for (int i = 2; i <= n; i++) {
                isPrime[i] = true;
            }

            // Sàng Eratosthenes để loại bỏ các bội số
            for (int p = 2; p * p <= n; p++) {
                if (isPrime[p]) {
                    for (int i = p * p; i <= n; i += p) {
                        isPrime[i] = false;
                    }
                }
            }

            return isPrime[n];
    }
}

