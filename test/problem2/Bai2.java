package problem2;

public class Bai2 {
    public static void main(String[] args) {
        System.out.println("Các số thỏa mãn điều kiện:");

        for (int num = 10000; num <= 9999999; num++) {
            if (isPrime(num) && isPalindrome(num) && hasAllPrimeDigits(num) && hasPrimeDigitSum(num)) {
                System.out.println(num);
            }
        }
    }

    // Kiểm tra xem một số có phải là số nguyên tố hay không
    public static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        if (num <= 3) {
            return true;
        }
        if (num % 2 == 0 || num % 3 == 0) {
            return false;
        }
        for (int i = 5; i * i <= num; i += 6) {
            if (num % i == 0 || num % (i + 2) == 0) {
                return false;
            }
        }
        return true;
    }

    // Kiểm tra xem một số có phải là số đối xứng (palindrome) hay không
    public static boolean isPalindrome(int num) {
        String numStr = Integer.toString(num);
        int length = numStr.length();
        for (int i = 0; i < length / 2; i++) {
            if (numStr.charAt(i) != numStr.charAt(length - 1 - i)) {
                return false;
            }
        }
        return true;
    }

    // Kiểm tra xem tất cả các chữ số của số đều là số nguyên tố
    public static boolean hasAllPrimeDigits(int num) {
        String numStr = Integer.toString(num);
        for (int i = 0; i < numStr.length(); i++) {
            int digit = Character.getNumericValue(numStr.charAt(i));
            if (!isPrime(digit)) {
                return false;
            }
        }
        return true;
    }

    // Kiểm tra xem tổng các chữ số của số có phải là số nguyên tố hay không
    public static boolean hasPrimeDigitSum(int num) {
        int sum = 0;
        while (num > 0) {
            int digit = num % 10;
            sum += digit;
            num /= 10;
        }
        return isPrime(sum);
    }
}

