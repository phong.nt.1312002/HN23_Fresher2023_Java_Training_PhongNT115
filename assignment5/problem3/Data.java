package problem3;

class Data {
    private int total = 0;

    public synchronized int getTotal() {
        return total;
    }

    public synchronized void updateTotal(int value) {
        total += value;
    }
}