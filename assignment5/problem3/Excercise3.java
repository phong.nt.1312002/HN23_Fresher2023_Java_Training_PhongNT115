package problem3;

public class    Excercise3 {
    public static void main(String[] args) {
        Data data = new Data();
        Thread thread1 = new Thread(new RandomNumberGenerator(data, true));
        Thread thread2 = new Thread(new RandomNumberGenerator(data, false));

        thread1.start();
        thread2.start();
    }
}




