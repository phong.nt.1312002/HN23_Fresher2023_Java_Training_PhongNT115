package problem3;

class RandomNumberGenerator implements Runnable {
    private Data data;
    private boolean isPositive;

    public RandomNumberGenerator(Data data, boolean isPositive) {
        this.data = data;
        this.isPositive = isPositive;
    }

    @Override
    public void run() {
        while (true) {
            int randomNumber = generateRandomNumber();
            data.updateTotal(randomNumber);
            System.out.println("Thread " + Thread.currentThread().getId() + ": Số ngẫu nhiên = " + randomNumber + ", Total = " + data.getTotal());
            if (data.getTotal() <= -100 || data.getTotal() >= 100) {
                System.out.println("Thread " + Thread.currentThread().getId() + " dừng.");
                break;
            }
        }
    }

    private int generateRandomNumber() {
        int min = isPositive ? 0 : -100;
        int max = isPositive ? 100 : 0;
        return min + (int) (Math.random() * (max - min + 1));
    }
}
