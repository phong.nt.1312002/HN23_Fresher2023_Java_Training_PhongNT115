package problem1;
public class Excercise1 {

    private static Integer sharedData = null;
    private static final Object lock = new Object();

    public static void main(String[] args) {
        Thread thread1 = new Thread(new Thread1());
        Thread thread2 = new Thread(new Thread2());

        thread1.start();
        thread2.start();
    }

    static class Thread1 implements Runnable {
        @Override
        public void run() {
            while (true) {
                synchronized (lock) {
                    sharedData = (int) (Math.random() * 20) + 1;
                    System.out.println("Thread 1 sinh số: " + sharedData);
                    lock.notify();
                }

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class Thread2 implements Runnable {
        @Override
        public void run() {
            while (true) {
                synchronized (lock) {
                    while (sharedData == null) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    int square = sharedData * sharedData;
                    System.out.println("Thread 2 bình phương của " + sharedData + " là: " + square);
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}





