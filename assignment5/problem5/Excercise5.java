package problem5;

public class Excercise5 {
    public static void main(String[] args) {
        Object monitor = new Object();
        String[] names = {"Nguyen Van Huan", "Nguen Linh Duc", "Nguyen Van Tan", "Vu Viet Tung", "Tran Trung Nghia"};
        String[] hometowns = {"Ha Noi", "Hung Yen", "Thanh Hoa", "Ha Tinh", "Quang Ninh"};

        Thread thread1 = new Thread(() -> {
            synchronized (monitor) {
                for (int i = 0; i < 5; i++) {
                    System.out.println("Tên: " + names[i]);
                    monitor.notify();
                    try {
                        if (i < 4) {
                            monitor.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Thread thread2 = new Thread(() -> {
            synchronized (monitor) {
                for (int i = 0; i < 5; i++) {
                    System.out.println("Quê hương: " + hometowns[i]);
                    monitor.notify();
                    try {
                        if (i < 4) {
                            monitor.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Hoàn thành!");
    }
}
