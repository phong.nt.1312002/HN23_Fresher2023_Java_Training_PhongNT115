package problem4;

import java.util.ArrayList;
import java.util.Random;

class Thread2 extends Thread {
    private ArrayList<Character> list2 = new ArrayList<>();
    private int count = 0;
    private Random random = new Random();

    public ArrayList<Character> getList2() {
        return list2;
    }

    @Override
    public void run() {
        while (count < 10) {
            char randomChar = (char) ('a' + random.nextInt(26));
            list2.add(randomChar);
            count++;

            try {
                Thread.sleep(2000); // Dừng 2 giây
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
