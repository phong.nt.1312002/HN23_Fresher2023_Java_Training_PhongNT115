package problem4;

import java.util.ArrayList;
import java.util.Random;

public class Excercise4 {
    public static void main(String[] args) {
        Thread1 t1 = new Thread1();
        Thread2 t2 = new Thread2();

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Kết quả của Thread1 (số ngẫu nhiên): " + t1.getList1());
        System.out.println("Kết quả của Thread2 (ký tự ngẫu nhiên): " + t2.getList2());
    }
}




