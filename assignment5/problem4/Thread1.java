package problem4;

import java.util.ArrayList;
import java.util.Random;

class Thread1 extends Thread {
    private ArrayList<Integer> list1 = new ArrayList<>();
    private int count = 0;
    private Random random = new Random();

    public ArrayList<Integer> getList1() {
        return list1;
    }

    @Override
    public void run() {
        while (count < 10) {
            int randomNumber = random.nextInt(101);
            list1.add(randomNumber);
            count++;

            try {
                Thread.sleep(1000); // Dừng 1 giây
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
