package problem2;

class CharacterGenerator implements Runnable {

    public char randomChar;
    @Override
    public void run() {
        while (!Thread.interrupted()) {
            randomChar = (char) ('a' + (int) (Math.random() * 26));
            synchronized (this) {
                System.out.println("Sinh ký tự ngẫu nhiên: " + randomChar);
                this.notify(); // Thông báo cho luồng 2 tiếp tục
                try {
                    Thread.sleep(2000); // Dừng 2 giây
                    this.wait(); // Chờ luồng 2
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public char getLastRandomChar() {
        return randomChar;
    }
}

