package problem2;

public class Excercise2 {
    public static void main(String[] args) {
        CharacterGenerator characterGenerator = new CharacterGenerator();
        CharacterConverter characterConverter = new CharacterConverter(characterGenerator);

        Thread thread1 = new Thread(characterGenerator);
        Thread thread2 = new Thread(characterConverter);

        thread1.start();
        thread2.start();

        try {
            Thread.sleep(20000); // Dừng 20 giây
            thread1.interrupt(); // Dừng luồng 1
            thread2.interrupt(); // Dừng luồng 2
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


