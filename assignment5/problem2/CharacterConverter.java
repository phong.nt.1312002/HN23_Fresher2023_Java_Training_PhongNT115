package problem2;

class CharacterConverter implements Runnable {
    private CharacterGenerator characterGenerator;

    public CharacterConverter(CharacterGenerator characterGenerator) {
        this.characterGenerator = characterGenerator;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            synchronized (characterGenerator) {
                try {
                    characterGenerator.wait(); // Chờ luồng 1
                    char generatedChar = characterGenerator.getLastRandomChar();
                    char upperChar = Character.toUpperCase(generatedChar);
                    System.out.println("Ký tự viết hoa: " + upperChar);
                    characterGenerator.notify(); // Thông báo cho luồng 1 tiếp tục
                    Thread.sleep(1000); // Dừng 1 giây
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}

