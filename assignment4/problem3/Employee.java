package problem3;
public class Employee {
    private static int counter = 1;
    private int id;
    private String name;
    private String gender;
    private String dateOfBirth;
    private String phoneNumber;
    private String educationLevel;

    public Employee(String name, String gender, String dateOfBirth, String phoneNumber, String educationLevel) {
        this.id = counter++;
        this.name = name;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.phoneNumber = phoneNumber;
        this.educationLevel = educationLevel;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDob(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public void setInformation(){

    }

    @Override
    public String toString() {
        return "ID: " + id + "\nName: " + name + "\nGender: " + gender + "\nDOB: " + dateOfBirth + "\nPhone Number: " + phoneNumber + "\nEducation Level: " + educationLevel;
    }
}






