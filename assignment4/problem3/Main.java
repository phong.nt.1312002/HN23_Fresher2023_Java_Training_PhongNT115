package problem3;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Menu:");
            System.out.println("1. Add employee");
            System.out.println("2. Display employee information");
            System.out.println("3. Update employee information");
            System.out.println("4. Find employee by ID or name");
            System.out.println("5. Exit");
            System.out.print("Enter your choice : ");

            String choice = scanner.nextLine();

            switch (choice) {
                case "add":
                    System.out.print("Enter name: ");
                    String name = scanner.nextLine();
                    System.out.print("Enter gender (Male/Female): ");
                    String gender = scanner.nextLine();
                    while (true) {
                        if (gender.equalsIgnoreCase("Male")) break;
                        else if (gender.equalsIgnoreCase("FeMale")) break;
                        else {
                            System.out.println("Wrong Gender!");
                            System.out.print("Enter gender (Male/Female): ");
                            gender = scanner.nextLine();
                        }
                    }
                    System.out.print("Enter Date Of Birth (dd/mm/yyyy): ");
                    String dateOfBirth = scanner.nextLine();
                    while (dateOfBirth.length() != 10 || dateOfBirth.charAt(2) != '/' || dateOfBirth.charAt(5) != '/') {
                        System.out.println("Wrong Date of Birth!");
                        System.out.print("Enter Date Of Birth (dd/mm/yyyy): ");
                        dateOfBirth = scanner.nextLine();
                    }
                    System.out.print("Enter phone number: ");
                    String phoneNumber = scanner.nextLine();
                    System.out.print("Enter education level (Trung cấp/Cao đẳng/Đại học): ");
                    String educationLevel = scanner.nextLine();
                    while (true) {
                        if (educationLevel.equalsIgnoreCase("Trung cấp")) break;
                        else if (educationLevel.equalsIgnoreCase("Cao Đẳng")) break;
                        else if (educationLevel.equalsIgnoreCase("Đại học")) break;
                        else if (educationLevel.equalsIgnoreCase("")) break;
                        else {
                            System.out.println("Wrong Education Level!");
                            System.out.print("Enter education level (Trung cấp/Cao đẳng/Đại học): ");
                            educationLevel = scanner.nextLine();
                        }
                    }

                    employees.add(new Employee(name, gender, dateOfBirth, phoneNumber, educationLevel));
                    System.out.println("Employee added.");
                    break;

                case "display":
                    if (employees.isEmpty()) {
                        System.out.println("Not found information.");
                    } else {
                        for (Employee employee : employees) {
                            System.out.println(employee);
                            System.out.println("----------");
                        }
                    }
                    break;

                case "update":
                    if (employees.isEmpty()) {
                        System.out.println("Not found information.");
                    } else {
                        System.out.print("Enter employee ID: ");
                        int id = scanner.nextInt();
                        scanner.nextLine();
                        Employee employeeToUpdate = null;
                        for (Employee employee : employees) {
                            if (employee.getId() == id) {
                                employeeToUpdate = employee;
                                break;
                            }
                        }
                        if (employeeToUpdate != null) {
                            System.out.print("Enter name: ");
                            name = scanner.nextLine();
                            employeeToUpdate.setName(name);
                            employeeToUpdate.setName(name);
                            System.out.print("Enter gender (Male/Female): ");
                            gender = scanner.nextLine();
                            while (true) {
                                if (gender.equalsIgnoreCase("Male")) break;
                                else if (gender.equalsIgnoreCase("FeMale")) break;
                                else {
                                    System.out.println("Wrong Gender!");
                                    System.out.print("Enter gender (Male/Female): ");
                                    gender = scanner.nextLine();
                                }
                            }
                            employeeToUpdate.setGender(gender);
                            System.out.print("Enter Date Of Birth (dd/mm/yyyy): ");
                            dateOfBirth = scanner.nextLine();
                            while (dateOfBirth.length() != 10 || dateOfBirth.charAt(2) != '/' || dateOfBirth.charAt(5) != '/') {
                                System.out.println("Wrong Date of Birth!");
                                System.out.print("Enter Date Of Birth (dd/mm/yyyy): ");
                                dateOfBirth = scanner.nextLine();
                            }
                            employeeToUpdate.setDob(dateOfBirth);
                            System.out.print("Enter phone number: ");
                            phoneNumber = scanner.nextLine();
                            employeeToUpdate.setPhoneNumber(phoneNumber);
                            System.out.print("Enter education level (Trung cấp/Cao đẳng/Đại học): ");
                            educationLevel = scanner.nextLine();
                            while (true) {
                                if (educationLevel.equalsIgnoreCase("Trung cấp")) break;
                                else if (educationLevel.equalsIgnoreCase("Cao Đẳng")) break;
                                else if (educationLevel.equalsIgnoreCase("Đại học")) break;
                                else if (educationLevel.equalsIgnoreCase("")) break;
                                else {
                                    System.out.println("Wrong Education Level!");
                                    System.out.print("Enter education level (Trung cấp/Cao đẳng/Đại học): ");
                                    gender = scanner.nextLine();
                                }
                            }
                            employeeToUpdate.setEducationLevel(educationLevel);
                            System.out.println("Employee information updated.");
                            break;
                        } else {
                            System.out.println("Employee not found.");
                        }
                    }
                    break;

                case "find":
                    if (employees.isEmpty()) {
                        System.out.println("Not found information.");
                    } else {
                        System.out.print("Enter employee ID or name: ");
                        String searchTerm = scanner.nextLine();
                        while (searchTerm.isEmpty()) {
                            System.out.println("Please enter your information!");
                            System.out.println("Enter employee ID or name: ");
                            searchTerm = scanner.nextLine();
                        }
                        boolean found = false;
                        for (Employee employee : employees) {
                            if (String.valueOf(employee.getId()).equals(searchTerm) || employee.getName().equalsIgnoreCase(searchTerm)) {
                                System.out.println(employee);
                                System.out.println("----------");
                                found = true;
                            }
                        }
                        if (!found) {
                            System.out.println("Not found information.");
                        }
                    }
                    break;

                case "exit":
                    System.out.println("Exiting the program.");
                    scanner.close();
                    System.exit(0);
                    break;

                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }
}
