package problem2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Map<Integer, SinhVien> sinhVienMap = new HashMap<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Menu:");
            System.out.println("1. Nhập thông tin sinh viên");
            System.out.println("2. In thông tin sinh viên");
            System.out.println("3. Tìm kiếm sinh viên");
            System.out.println("4. Thoát");
            System.out.print("Chọn chức năng (1/2/3/4): ");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    System.out.print("Nhập số lượng sinh viên: ");
                    int num = scanner.nextInt();
                    scanner.nextLine();

                    for (int i = 0; i < num; i++) {
                        System.out.println("Nhập thông tin cho sinh viên thứ " + (i + 1));
                        System.out.print("RollNo: ");
                        int rollNo = scanner.nextInt();
                        scanner.nextLine();
                        System.out.print("Name: ");
                        String name = scanner.nextLine();
                        System.out.print("Sex (male/female/else): ");
                        String sex = scanner.nextLine();
                        while (true) {
                            if (sex.equalsIgnoreCase("Male")) break;
                            else if (sex.equalsIgnoreCase("FeMale")) break;
                            else if (sex.equalsIgnoreCase("else"))break;
                            else {
                                System.out.println("Wrong Sex!");
                                System.out.print("Sex (male/female/else): ");
                                sex = scanner.nextLine();
                            }
                        }
                        System.out.print("Age: ");
                        int age = scanner.nextInt();
                        while (age<=0){
                            System.out.println("Age is not valid!");
                            System.out.print("Age: ");
                            age = scanner.nextInt();
                        }
                        scanner.nextLine();
                        System.out.print("Email: ");
                        String email = scanner.nextLine();
                        System.out.print("Address: ");
                        String address = scanner.nextLine();

                        SinhVien sinhVien = new SinhVien(rollNo, name, sex, age, email, address);
                        sinhVienMap.put(rollNo, sinhVien);
                        System.out.println("Sinh viên đã được thêm vào danh sách.");
                    }
                    break;

                case 2:
                    System.out.println("Danh sách sinh viên:");
                    if(sinhVienMap.isEmpty()){
                        System.out.println("Không có thông tin danh sách sinh viên");
                    }
                    else {
                        for (SinhVien sinhVien : sinhVienMap.values()) {
                            System.out.println(sinhVien.toString());
                        }
                    }
                    break;

                case 3:
                    System.out.print("Nhập RollNo để tìm kiếm: ");
                    int rollNoToSearch = scanner.nextInt();
                    SinhVien foundSinhVien = sinhVienMap.get(rollNoToSearch);
                    if (foundSinhVien != null) {
                        System.out.println("Thông tin sinh viên có RollNo " + rollNoToSearch + ":");
                        System.out.println(foundSinhVien.toString());
                    } else {
                        System.out.println("Không tìm thấy sinh viên với RollNo " + rollNoToSearch);
                    }
                    break;

                case 4:
                    System.out.println("Thoát chương trình.");
                    scanner.close();
                    System.exit(0);
                    break;

                default:
                    System.out.println("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
            }
        }
    }
}
