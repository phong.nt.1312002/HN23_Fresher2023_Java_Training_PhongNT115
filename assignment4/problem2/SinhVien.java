package problem2;
public class SinhVien {
    private int rollNo;
    private String name;
    private String sex;
    private int age;
    private String email;
    private String address;

    public SinhVien(int rollNo, String name, String sex, int age, String email, String address) {
        this.rollNo = rollNo;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.email = email;
        this.address = address;
    }

    public int getRollNo() {
        return rollNo;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "RollNo: " + rollNo + ", Name: " + name + ", Sex: " + sex + ", Age: " + age + ", Email: " + email + ", Address: " + address;
    }
}
