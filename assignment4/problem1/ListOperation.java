package problem1;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Scanner;
public class ListOperation {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            // Nhập số lượng phần tử cho mảng colors
            System.out.print("Nhập số lượng phần tử cho mảng colors: ");
            int numColors = scanner.nextInt();
            scanner.nextLine();

            // Khai báo và thêm các phần tử cho mảng colors
            String[] colors = new String[numColors];
            for (int i = 0; i < numColors; i++) {
                System.out.print("Nhập màu thứ " + (i + 1) + ": ");
                colors[i] = scanner.nextLine();
            }

            // Khai báo mảng colors2 và thêm các phần tử cho nó
            System.out.print("Nhập số lượng phần tử cho mảng colors2: ");
            int numColors2 = scanner.nextInt();
            scanner.nextLine();

            String[] colors2 = new String[numColors2];
            for (int i = 0; i < numColors2; i++) {
                System.out.print("Nhập màu thứ " + (i + 1) + " cho mảng colors2: ");
                colors2[i] = scanner.nextLine();
            }

            // Tạo danh sách list và list2 với các phần tử từ mảng colors và colors2
            List<String> list = new ArrayList<>();
            Collections.addAll(list, colors);

            List<String> list2 = new ArrayList<>();
            Collections.addAll(list2, colors2);

            // Thêm phần tử từ list2 vào list
            list.addAll(list2);
            System.out.println("list sau khi thêm phần tử từ list2: " + list);

            // Xóa các phần tử từ list2 để biến nó thành danh sách rỗng
            list2.clear();
            System.out.println("list2 sau khi xóa: " + list2);

            // Chuyển các phần tử trong list thành ký tự in hoa
            for (int i = 0; i < list.size(); i++) {
                list.set(i, list.get(i).toUpperCase());
            }
            System.out.println("list sau khi chuyển thành ký tự in hoa: " + list);

            // Xóa các phần tử trong list từ phần tử thứ 4 đến phần tử thứ 6
            try{
                list.subList(3, 6).clear();
                System.out.println("list sau khi xóa từ phần tử thứ 4 đến phần tử thứ 6: " + list);
            }
            catch (IndexOutOfBoundsException e){
                System.out.println("Lỗi: Truy cập vượt quá số phần tử của mảng");
            }
            // Đảo ngược list
            Collections.reverse(list);
            System.out.println("list sau khi đảo ngược: " + list);

            scanner.close();
        }
}
